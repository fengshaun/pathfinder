# in this implementation of the priority queue version of Dijkstra's shortest
# path algorithm. We'll assume a distance of 1 between all the nodes for
# simplicity. The implementation would be the same either way.

# python's queue.PriorityQueue is not useful here, so I'll make my own
class PQueue:
    def __init__(self):
        self._list = []

    def put(self, obj):
        self._list.append(obj)

    def get(self):
        self._list.sort()
        temp = self._list[0]
        del self._list[0]
        return temp

    # a window into the soul of PQueue
    def search(self, pred):
        for i in self._list:
            if pred(i):
                return i

    def empty(self):
        return len(self._list) == 0

class Node:
    def __init__(self, name, dist=float("inf"), vis=False, neigh=[]):
        self.name     = name
        self.distance = dist
        self.visited  = vis
        self.neighbours = neigh

    def __str__(self):
        return "N(%s:%s -> %s)" % (self.name, self.distance, self.neighbours)

    def __repr__(self):
        return self.__str__()

    def __lt__(self, other):
        return self.distance < other.distance

def get_node(name, graph):
    for i in graph:
        if i.name == name:
            return i

def generate_graph(graph):
    return [Node(path[0], float("inf"), False, path[1:]) for path in graph]

if __name__ == "__main__":
    # the format of the list given to generate_graph is this:
    # ["<Node letter><list of neighbours>", "..."]
    # so "abc" becomes Node a with neighbours Node b and Node c
    # a lone "m" indicates a node without neighbours (a final node)
    # also, this is a uni-directional graph for the sake of simplicity
    # not much needs to be added to make this work with bi-directional graphs
    g = generate_graph(["abc",
                        "bcd",
                        "ce",
                        "ef",
                        "df",
                        "fg",
                        "ghi",
                        "hj",
                        "jk",
                        "kml",
                        "m",
                        "in",
                        "li",
                        "n"])
    g[0].distance = 0

    unvisited = PQueue()
    for n in g:
        unvisited.put(n)

    while not unvisited.empty():
        current_node = unvisited.get()
        current_node.visited = True
        print(current_node)

        for n in current_node.neighbours:
            # this is a simplification. adding edges and distances isn't
            # hard, it would just distract from the concept
            neighbour = get_node(n, g)
            if not neighbour is None:
                if neighbour.distance > current_node.distance + 1:
                    unvisited.search(lambda x: x.name == neighbour.name).distance = current_node.distance + 1
                print("\t%s" % neighbour)
            else:
                print("\t%s is visited" % n)

    print(g)
