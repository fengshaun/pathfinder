# 0 means pathable, 1 means blocked
# I don't know why, but this is the way it's going to be
matrix = [
    [1,1,1,1,1,1,1,1,1,1],
    [1,0,0,1,1,1,0,0,0,1],
    [1,0,1,1,1,1,0,0,0,1],
    [1,0,0,1,1,1,0,1,1,1],
    [1,0,0,1,1,0,0,1,1,1],
    [1,1,0,0,0,0,1,1,1,1],
    [1,1,0,0,0,0,0,1,1,1],
    [1,1,1,0,0,1,0,0,0,1],
    [1,1,0,0,0,0,0,0,0,1],
    [1,1,1,1,1,1,1,1,1,1],
]

matrix_start_coord = (2, 1) # start node
matrix_end_coord   = (8, 8)

class Node:
    def __init__(self, coord, p=True, t=False, c=0):
        self.pathable = p
        self.traversed = t
        self.counter = c
        self.set_coord(coord)

    # coord is a tuple in the form of (x, y)
    def set_coord(self, c):
        self.x = c[0]
        self.y = c[1]

    # WARNING: THIS ONLY COMPARES THE COORDINATES
    # you have to take care of counter separately
    def __eq__(self, other):
        if (self.x == other.x) and (self.y == other.y):
            return True
        return False

    def __lt__(self, other):
        if self.x != other.x:
            return self.x < other.x
        else:
            return self.y < other.y

    def __str__(self):
        return "(%d, %d, %d)" % (self.x, self.y, self.counter)

    def __repr__(self):
        return self.__str__()

def add_adjacent_nodes(node, path):
    # I'll be breaking the 80 char limit recommendation here, sorry!
    # TODO: the bounds checking feels crude, could be improved
    if node.x > 0:
        path.append(Node((node.x-1, node.y),   matrix[node.x-1][node.y].pathable, False, node.counter+1)) #left
    if node.x < len(matrix[0]) - 1:
        path.append(Node((node.x+1, node.y),   matrix[node.x+1][node.y].pathable, False, node.counter+1)) #right
    if node.y > 0:
        path.append(Node((node.x,   node.y-1), matrix[node.x][node.y-1].pathable, False, node.counter+1)) #top
    if node.y < len(matrix) - 1:
        path.append(Node((node.x,   node.y+1), matrix[node.x][node.y+1].pathable, False, node.counter+1)) #bottom

    # this is really not needed since path is modified in-place,
    # but it's provided more for stylistic purposes so all helper functions
    # behave similarly in what they return
    return path

def dedup_path(path):
    seen = []
    for node in path:
        if not node.pathable:
            # quietly remove the nodes that are blocked/un-pathable
            # is unpathable a word? I hope so
            pass
        elif node not in seen:
            seen.append(node)
        else:
            # now we compare the counter
            for i in filter(lambda x: x == node, seen):
                if node.counter < i.counter:
                    # show the already-present node the door
                    # don't need him anymore. OR just politely
                    # ask it to get out of our processing list
                    # obviously this could be better optimized,
                    # but that's for another time
                    seen.remove(i)
                    seen.append(node)
            # we may have duplicate identical entries at this point,
            # so a cleanup is due
            #seen = list(set(seen))

    return seen

if __name__ == "__main__":
    # converting the above matrix to a more process-able form
    for x in range(0, 10):
        for y in range(0, 10):
            matrix[x][y] = Node((x, y), matrix[x][y] == 0)

    path = [Node(matrix_end_coord, True)]

    # and now the meat, minus the potatoes!
    # a for-loop unfortunately doesn't work here since the path[] changes
    # all the time
    i = 0
    while True:
        #print("%d => %s" % (i, path))
        if i >= len(path):
            break

        temp = path
        path = dedup_path(add_adjacent_nodes(temp[i], temp))

        if Node(matrix_start_coord) in path:
            break # we're done

        i += 1

    # try to prettify the output to make sense of it from end to start
    path.sort()
    path.reverse()
    print(path)
