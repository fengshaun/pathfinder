\documentclass[12pt]{article} % Default font size is 12pt, it can be changed here

\usepackage{geometry}
\geometry{a4paper}

\usepackage{graphicx}
\usepackage{verbatim}
\usepackage{float} % Allows putting an [H] in \begin{figure} to specify the exact location of the figure

\linespread{1} % Line spacing
\setlength\parindent{0pt} % remove all indentation from paragraphs
\setlength\parskip{1em} % add a skip between paragraphs

\graphicspath{{figures/}} % Specifies the directory where pictures are stored

\title{Thoughts on the Shortest Path Problem and Marine Navigation}
\author{Armin Moradi}
\date{19/12/2014}

\begin{document}

\maketitle
\vfill
\tableofcontents

\section{Problem}
Given \ref{fig:original} below, what would be the best approach to finding the shortest path from point A to point B, assuming the shaded areas are unpathable/blocked.

\begin{figure}[H]
\center{\includegraphics[width=0.8\linewidth]{figure1}}
\caption{The original problem}
\label{fig:original}
\end{figure}

\section{Introduction}
Before diving into the proposed problem, it is worth exploring the well-known path-finding algorithms in abstract form. When thinking about a shortest-path problem, it is helpful to think either in terms of nodes in graph, or a two-dimentional matrix. When thinking about this problem in two-dimentional space, one sample implementation could be seen in \verb+simple.py+.

However, when we move beyond a 10$\times$10 matrix, this algorithms becomes massively burdensome and slow. What now?

A better way to think about the shortest-path problem is, perhaps, to think of it in terms of nodes in a connected graph. In this case, the nodes are connected by positively-weighted edges (we will not concern ourselves with negatively-weighted edges as distances cannot have negative values in the real world). Then, we can focus on finding a path through various edges from start to finish:

%figure2 (sample graph with nodes)
\begin{figure}[H]
\center{\includegraphics[width=0.8\linewidth]{figure2}}
\caption{A sample graph structure on top of the map that can be generated to enable path-finding}
\label{fig:samplegraph}
\end{figure}

One of the most well-known algorithms that deals with such an issue is the Dijkstra's algorithm published in 1959 which has been revised and improved over time for various specific applications. The ``priority queue'' with a uni-directional graph variation of this algorithm is implemented \verb+dijkstra.py+.

This algorithm has an efficiency of O($n^{2}$) where n is the number of vertices in the graph. There have been multiple improvements and variations over the original Dijkstra's algorithm the most relevant of which is the A* algorithm which uses a Best-First Search (BFS) approach to eliminate a number of extraneous routes from the original Dijkstra's algorithm.

Other algorithms such as Floyd-Warshall, Johnson's, and Bellman-Ford are not going to be discussed since they trade efficiency for versatility of being able to deal with negative-weight edges. In our problem, negative-weight edges are undefined and thus these methods are inferior to the ones discussed.

\section{Proposed Solution}
\label{sol}
Going back to the problem of boat navigation, it is worth noting that unlike road and highway on solid ground, a sensible graph structure is not already present on water; there are no roads, no intersections, and no way of differentiating between ``blocks'' of water. This would make the problem of shortest-path infinitely difficult and it would seem that a truly optimal solution may never be reached. However, upon a closer look, the water \textit{can} be sectionized into bigger pieces to assist with the speed and efficiency of a reasonably simple shortest-path algorithm. This is how one would go about simplifying the ocean and preparing it for a two-dimentional graph structure.

%figure3 (sectionized)
\begin{figure}[H]
\center{\includegraphics[width=0.8\linewidth]{figure3}}
\caption{The map divided into digestible chunks/blocks}
\label{fig:sectionized}
\end{figure}

The next step would be to generate a graph structure from this two-dimentional ``array'' where pathable ``blocks'' contain edges. Please note that the darker dots denote centers of chunks and are considered nodes. Connection between these nodes are considered edges. These are what would be used to compute the shortest path.

%figure4 (path points/edges)
\begin{figure}[H]
\center{\includegraphics[width=0.8\linewidth]{figure4}}
\caption{a graph structure from map chunks}
\label{fig:sectionizedgraph}
\end{figure}

Using A* algorithm with BFS approach, a crude/unoptimized shortest path could be found that roughly resembles a path that a ship might take. the ``blocks'' in this graph can be as small or as large as reasonable. Please note that at this point, we only have an approximation of the shortest path; there may be a shorter path, but given the complexity of marine navigation, a truly correct optimal path would take far longer to find than is perhaps worth, especially given computational limitation of embedded systems (see section~\ref{adv} for a longer discussion).

\subsection{Forward and Backward Optimization}
Given a crude shortest path from point A to point B, we can optimize and smoothen the path to achieve a better approximation of the shortest path possible. Various optimization techniques could be used such as trying to connect alternating nodes (thus creating a triangular form). Multiple passes of this technique with subsequently shorter edges (e.g. divide the edges in two and add a node in-between after each pass) could result in a finer path within a reasonable time (a similar idea to contraction hierarchies).

Another way of optimizing not the path, but the entire solution for speed would be to divide the entire map into big blocks and trying to find the shortest path from point A to point B using the resulting graph, but over the duration of travel, trying to calculate a finer path using the same methods described in section~\ref{sol} while assuming start and finish to be nodes C and D.

While the above optimizations propose forward optimizations (i.e. optimizing the already-found path), it is also possible to fine-tune the generated graph by pruning extraneous paths (i.e. paths resulting in the same point) before attempting to find the shortest path from longer pruned sub-paths.

There are many many other possibilities when it comes to optimizations from preprocessing of the graph and adding shortcuts to outsourcing predefined shortest-path routes to a central server to be accessed if possible. A detailed look at optimization technique would be a better fit for another paper. However, one optimization that would drastically improve the performance/accuracy trade-off would be to dynamically allocate chunk sizes before generating a graph structure. In this case, the chunk size would be computed as inversely proportional to the complexity of the terrain:

%figure6 proportional chunk size
\begin{figure}[H]
\center{\includegraphics[width=0.8\linewidth]{figure5}}
\caption{Terrain complexity and chunk-size should be inversely-propotional}
\label{fig:proportional}
\end{figure}

This allocation method would not only resolve the accuracy problem mentioned earlier, but would also increase the efficiency of the solution in waters that have no unpathable sections (clear waters as long as the eye can see would simply be one huge chunk instead of multiple smaller chunks which would otherwise consume precious processing power).\footnote{It is worth noting that the complexity of the terrain can be calculated beforehand using image-recognition technology coupled with satellite images.}\footnote{Additionally, chunking could be done rather quickly and efficiently by first dividing the entire map in fixed-sized big chunks, then using calculated complexity, the chunks closest to the ground/water border could be further divided by 2 (with each ``degree'' of complexity, we would divide the chunk including that area in 2) to increase ``resolution'' of the generated graph structure. These optimizations, as exciting and interesting as they are, are outside the scope of this paper, even though two footnotes were spent in digression.}

\subsection{Advantages and Disadvantages}
\label{adv}
The proposed solution is certainly not the best solution to the marine navigation problem and could certainly be improved upon. One of the advantages of this solution is, that is fast and could potentially run on embedded systems at reasonable pace. This is due to the chunking and simplifying of the map which takes place before the actual path-finding occurs. However, what could be construed as its advantages also results in its most evident disadvantage: this proposed solution only \textit{approximates} the optimal path and could, depending on the chunk size, yield a different path than the optimal path. Depending on the power of the system on which this algorithm would run, the chunking size could be reduced to achieve a better approximation.

\end{document}
